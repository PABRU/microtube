/*
HELPERS :
 */
function getElem(selector, context) {
    return (typeof selector === 'string') ? (context || document).querySelector(selector) : selector;
}

function formatDate($date){
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var date;
    if(typeof $date === ("string" || "number")){
        date = new Date($date);
    } else{
        date = new Date();
    }

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function limitText($str, $length){
    if(!typeof $length == "number" || !typeof $str == "string")
        return false;

    var trimmedString = $str.substr(0, $length);
    return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + "...";
}

/*
PROPERTIES :
 */
const apiBase = "https://www.googleapis.com/youtube/v3/search";
const apiKey = "AIzaSyDHzv3ZAERZ3KhYTgWQ5ItbyrBOsoWhaX0";
let result = {
    videos: [],
    query: '',
    pageToken : null,
    nextPageToken: null,
    prevPageToken: null,
    regionCode: null,
    pageInfo: {
        totalResults: null,
        resultsPerPage: null
    }
};

/*
LAUNCH EVENT HANDLER :
 */
$(document).ready(function(){
    $(".pa_search_form").on("submit", function(e){
        e.preventDefault();
        getVideosByKeyword('#search-youtube');
    });

    $("body").on('click', '.video img', function(){
        let id = $(this).parent().attr("id");
        let video = getVideoById(id);
        setPlayerContent(video);
    });
});


/*
FUNCTIONS :
 */
function getVideoById(id){
    return result.videos.find(function(video){
        return video.id == id;
    });

}

function setVideo($item){
    let video = {};
    video.id = $item.id.videoId;
    video.title = $item.snippet.title;
    video.description = $item.snippet.description;
    video.shortDescription = limitText($item.snippet.description, 150);
    video.publishedAt = formatDate($item.snippet.publishedAt);
    video.channelId = $item.snippet.channelId;
    video.channelTitle = $item.snippet.channelTitle;
    video.thumbnail = $item.snippet.thumbnails.high.url;
    video.embed = "https://www.youtube.com/embed/" + video.id;

    result.videos.push(video);

    return `<div class="video" id="${video.id}">
                <img class="img-responsive" src="${video.thumbnail}" alt=""/>
                <h4 class="video_title">${video.title}</h4>
                By <i><a target="_blank" href="https://www.youtube.com/channel/${video.channelId}" class="channel_title">${video.channelTitle}</a></i>
                On ${video.publishedAt}<br/>
                <small>${video.shortDescription}</small>
            </div>
            <hr/>`;
}

function setResult(data){
    result.pageInfo = data.pageInfo;
    result.nextPageToken = data.nextPageToken;
    result.prevPageToken = data.prevPageToken;
    result.regionCode = data.regionCode;
    result.videos = [];
    console.log("TABLEAU RESULT");
    console.log(result);
}

function getVideosByKeyword($input){
    let $keyword = getElem($input).value;

    $.ajax({
        url: apiBase,
        type: 'GET',
        data: {
            part: 'snippet',
            q: $keyword,
            type: 'video',
            maxResults: 10,
            key: apiKey
        },
        dataType: 'json'
    }).done(function(response){
        result.query = $keyword;
        setResult(response);

        $("#youtube-results").html("");
        //get data of each item :
        for(var i in response.items){
            let video = setVideo(response.items[i]);
            $("#youtube-results").append(video);
            if(i == 0){
                setPlayerContent(result.videos[0]);
            }
        }
    }).fail(function() {
        $("#player").html("The search failed ! please try again !");
    })
}

function setPlayerContent(video){
    $("#player").html("");
    $(".pa_center_screen").hide();

    let template = `<div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/${video.id}?autoplay=1" allowfullscreen></iframe>
    </div>`;

    $("#player").append(template);
}